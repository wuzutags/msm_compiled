var g_loadImageURL = "<div align='center'><h3><img style='margin-top:10px;' src='assets/loading.gif' alt='...' width='40' height='40' /> <span style='color:white;'><br/>loading...</span></h3></div>";
function lt_eval_response(__setElemID, _xmlhttp)
{    
    if(__setElemID == null)
        return;
    
    var __matchExpr=/A_ScrptID_[A-z]*[0-9]*/gi;
    var __matchedIDs = _xmlhttp.responseText.match(__matchExpr);
    document.getElementById(__setElemID).innerHTML=_xmlhttp.responseText;
    if(__matchedIDs != null)
    {
        for( var i=0; i<__matchedIDs.length; ++i)
        { 
            if(document.getElementById(__matchedIDs[i]) != null)
                eval(document.getElementById(__matchedIDs[i]).innerHTML);
        }
    }
}

function lt_json_file(_xmlhttp, fetch){
	//console.log('hi');
	if(fetch){
		//alert(_xmlhttp.responseText);
		json_data = _xmlhttp.responseText;
		//menusound.stop(); 
		main.difficultyLevels();
	//call fxn to list
	}else{
		//alert("score submitted successfully!");
		//call fxn to make screen disappear
		//menusound.stop(); 
		main.loadingHighScores();
		
	}
}

function lt_ajax_getNavi(_elementID, getURL)
{
    document.getElementById(_elementID).innerHTML=g_loadImageURL;
    var xmlhttp = null;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari 
    	xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200){lt_eval_response(_elementID, xmlhttp);}
    }
    xmlhttp.open("GET", getURL, true);
    xmlhttp.send(null);
}

function lt_ajax_get(_elementID, getURL)
{                
    document.getElementById(_elementID).innerHTML=g_loadImageURL;
    var xmlhttp = null;

    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        { lt_eval_response(_elementID, xmlhttp); }
    }
    xmlhttp.open("GET", getURL, true);
    xmlhttp.send(null);
}

function lt_ajax_getNoLoadingImg(_elementID, getURL)
{
    var xmlhttp = null;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        { lt_eval_response(_elementID, xmlhttp); }
    }
    xmlhttp.open("GET", getURL, true);
    xmlhttp.send(null);
}

function lt_ajax_getNoHist(_elementID, getURL)
{
    document.getElementById(_elementID).innerHTML=g_loadImageURL;

    var xmlhttp = null;

    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            lt_eval_response(_elementID, xmlhttp);
        }
    }
    xmlhttp.open("GET", getURL, true);
    xmlhttp.send(null);
}

function lt_ajax_form_post(_elementID, postURL, postParams)
{    
    document.getElementById(_elementID).innerHTML=g_loadImageURL;
    
    var xmlhttp = null;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {lt_eval_response(_elementID, xmlhttp); }
    }
    xmlhttp.open("POST", postURL, true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send(postParams);
}


function lt_ajax_getNotAsync(getURL)
{
    var xmlhttpNA = null;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttpNA=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttpNA=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttpNA.onreadystatechange=function()
    {
        if (xmlhttpNA.readyState==4 && xmlhttpNA.status==200)
        {}
    }
    xmlhttpNA.open("GET", getURL, false);
    xmlhttpNA.send(null);
    return xmlhttpNA.responseText;
}

function lt_ajax_getNotAsync_1(getURL)
{
    var xmlhttpNA = null;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttpNA=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttpNA=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttpNA.onreadystatechange=function()
    {
        if (xmlhttpNA.readyState==4 && xmlhttpNA.status==200)
        { }
    }
    xmlhttpNA.open("GET", getURL, false);
    xmlhttpNA.send(null);
    return xmlhttpNA.responseText;
}

function lt_ajax_getNotAsync(_elementID, getURL)
{
    document.getElementById(_elementID).innerHTML=g_loadImageURL;
    var xmlhttp = null;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {}
    }
    xmlhttp.open("GET", getURL, false);
    xmlhttp.send(null);
    lt_eval_response(_elementID, xmlhttp);
}

function lt_ajax_form_postNotAsync(_elementID, postURL, postParams)
{
    document.getElementById(_elementID).innerHTML=g_loadImageURL;

    var xmlhttp = null;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {}
    }
    xmlhttp.open("POST", postURL, false);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send(postParams);
    lt_eval_response(_elementID, xmlhttp);
}

function lt_ajax_get_poll(_interval, _elementID, getURL)
{
    return setInterval(function(){
        lt_ajax_getNoHist(_elementID, getURL);
    }, _interval);
}


function lt_ajax_getMTN(getURL,fetch)
{                
    //document.getElementById(_elementID).innerHTML=g_loadImageURL;
    var xmlhttp = null;

    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
    	//alert(xmlhttp.status);
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {  
        	lt_json_file(xmlhttp,fetch); 
        	return xmlhttp;
        }
        else{
        	//alert('Server connection error');
        	json_data = -1;
        	//call fxn to show text here
        	menusound.stop(); 
        	main.ServerError();
        	return -1;
        }
    }
    xmlhttp.open("GET", getURL, true);
    xmlhttp.send(null);
}

function lt_ajax_form_postMTN(_elementID, postURL, postParams)
{    
    //document.getElementById(_elementID).innerHTML=g_loadImageURL;
    
    var xmlhttp = null;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {lt_eval_response(_elementID, xmlhttp); }
    }
    xmlhttp.open("POST", postURL, true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send(postParams);
}

function msieversion()
{
   var ua = window.navigator.userAgent
   var msie = ua.indexOf ( "MSIE " )

   if ( msie > 0 )      // If Internet Explorer, return version number
      return parseInt (ua.substring (msie+5, ua.indexOf (".", msie )))
   else                 // If another browser, return 0
      return 0

}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

//if ( msieversion() >= 4 )
//
//    document.write ( "This is Internet Explorer 4 or later" );
//
// else if ( msieversion() >= 3 )
//
//    document.write ( "This is Internet Explorer 3" );
//
// else
//
//    document.write ( "This is another browser" );