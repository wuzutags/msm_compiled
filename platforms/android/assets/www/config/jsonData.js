/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//var sql = require('sql.js');
//var sql = window.SQL ;//if you are in a browser
	
//var fs = require('fs');

//prepare fxns to fetch data and process gameplay variables

//moretext is the field that stores the selected items for the user now
//it is also stored in the selecteditem local value
//this feild contains the your selected items that will be checked against the essential items
//expected value of user will contain if a check shd be activated on items that are selected b4 procceding

var server_data;
var db;
var domain_path = "http://www.letigames.com/games/msm/";//"http://localhost/msm/"; //"https://legend11.mochahost.com/~wuzutags/games/mtn/main/";
//"http://www.letigames.com/games/mtn/main/"; "http://www.letigames.com/games/msm/";

var user_no = null;
var step = null;
var user_input = null;
var last_answer = null;
var more_step_text = null;
var prev_type = null;
var yes_step;
var no_step;
var isAuto = false;
var isServer = true;
var success_login = false;
var isTesting = true;
var anchor_step;
var current_knock_code;
var decision_choices = new Array();

//keys
/*
 * 0 = narration
 * 1 = decision
 * 2 = dailogue
 * 3 = advice
 * 4 = multichoice
 * 5 = singleChoice Icons
 * 6 = singleChoice large
 */
var gameplay_nodes;
var users;
var roles;
var codes;
var advice;
var essentials;
var essential_items = new Array();

//var user;

//$.get("demo_test.asp",function(data,status){
//    alert("Data: " + data + "\nStatus: " + status);
//});

if(isServer == false){
	//Load categories object JSON
	jQuery.getJSON("raw_gameplay_data.json", function(data){         
	    // data is yours parsed object
		//alert(data[0].text);
		gameplay_nodes = data;
		
		updateWrapper('gamedata', gameplay_nodes);
	}).success(function() {  })
	.error(function() { alert("Error connecting to Server"); })
	.complete(function() { });
	
	jQuery.getJSON("raw_users_data.json", function(data){         
	    // data is yours parsed object
		//alert(data[0].text);
		users = data;
		
		updateWrapper('users', users);
	});
	
	jQuery.getJSON("raw_role_data.json", function(data){         
	    // data is yours parsed object
		//alert(data[0].text);
		roles = data;
		
		updateWrapper('roles', roles);
	});
	
	jQuery.getJSON("raw_codes_data.json", function(data){         
	    // data is yours parsed object
		//alert(data[0].text);
		codes = data;
		
		updateWrapper('codes', codes);
	});
	
	jQuery.getJSON("raw_essential_items_data.json", function(data){         
	    // data is yours parsed object
		//alert(data[1].id);
		essentials = data;
		
		updateWrapper('essentials', essentials);
		console.log("ess id source: " + essentials[1].id);
		//alert(essential_items.join("|"));
	});
	
	jQuery.getJSON("raw_advice_items_data.json", function(data){         
	    // data is yours parsed object
		//alert(data[0].text);
		//advice = data;
	});

}

if(isTesting){
	domain_path = domain_path + "test/";
}

function getServerJSON_files(){
 
	//jQuery.getJSON(domain_path + "raw_users_data.json", function(data){         
	    // data is yours parsed object
		//alert(data[0].text);
		//users = data;
		
		//updateWrapper('users', users);
	//});
	//check if files are already persisted by checking user status
	var gme = fetchWrapper('user');
    if(gme != null){
    	//load jsons from memory
    	gameplay_nodes = fetchWrapper('gamedata');
    	roles = fetchWrapper('roles');
    	essentials = fetchWrapper('essentials');
    	//alert("bbb");
    	//gameplay_nodes = fetchWrapper('gamedata');
    	//update game data eventually so long as the internet was successful else use already stored
    	//game data
    	jQuery.getJSON(domain_path + "raw_gameplay_data.json", function(data){         
		    // data is yours parsed object
			//alert(data[0].text);
			gameplay_nodes = data;
			
			updateWrapper('gamedata', gameplay_nodes);
			
			setUser_analyticsWrapper(gme.nickname);
			
		}).success(function() {  })
		.error(function() { })
		.complete(function() { });
    }else{
	
		jQuery.getJSON(domain_path + "raw_gameplay_data.json", function(data){         
		    // data is yours parsed object
			//alert(data[0].text);
			gameplay_nodes = data;
			
			updateWrapper('gamedata', gameplay_nodes);
		}).success(function() {  })
		.error(function() { alert("Server Error! Check your internet connection!"); })
		.complete(function() { });
		
		jQuery.getJSON(domain_path + "raw_role_data.json", function(data){         
		    // data is yours parsed object
			//alert(data[0].text);
			roles = data;
			
			updateWrapper('roles', roles);
		});
		
	//	jQuery.getJSON(domain_path + "raw_codes_data.json", function(data){         
	//	    // data is yours parsed object
	//		//alert(data[0].text);
	//		codes = data;
	//		
	//		updateWrapper('codes', codes);
	//	});
		
		jQuery.getJSON(domain_path + "raw_essential_items_data.json", function(data){         
		    // data is yours parsed object
			//alert(data[1].id);
			essentials = data;
			
			updateWrapper('essentials', essentials);
			console.log("ess id source: " + essentials[1].id);
			//alert(essential_items.join("|"));
		});
    }
}

function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return null;
  else
    return results[1];
}

function getPicWithID(uniquefiles, currentfile, key){
	var idfile;
	//check if current file is in uniquefiles
	//split unique files into array
	//alert(currentfile + " key- " + key + " unique- " + uniquefiles);
if(uniquefiles != "0"){
	
	var uniquefiles_array = new Array();
	if(uniquefiles.indexOf("|") > -1)
		uniquefiles_array = uniquefiles.split("|");
	else
		uniquefiles_array.push(uniquefiles);
	//fetch id 
	var id = fetchWrapper(key);
	//alert(currentfile + " id- " + id + " unique- " + uniquefiles);
	//hash.replace("*", arrdata[0])
	for(var i = 0; i < uniquefiles_array.length; i++){
		if(currentfile.indexOf(uniquefiles_array[i]) > -1){
			idfile = currentfile.replace(".", id + ".");
			//alert(idfile + " key- " + id + " unique- " + uniquefiles);
			return idfile;
		}
	}

}
	return currentfile;
}

function getadvice(id) {
	//check code with server
	var rs = new Array();
	var search_arr = new Array();
	//jsonsql.query("select * from json.channel.items order by title desc", rs); 
	//rs = jsonsql.query("select * from advice where (id=='" + id + "')", advice); 
	if (id.indexOf("|") == -1){
		search_arr.push(id);
	}else{
		search_arr = id.split("|");
	}
	
	var match = 0;
	//check if items exist in id
	
	//change below
	for(var i = 0; i < advice.length; i++){
		var advice_id_arr = new Array();
		if (advice[i].id.indexOf("|") == -1){
			advice_id_arr.push(advice[i].id);
		}else{
			advice_id_arr = advice[i].id.split("|");
		}
		//var advice_id_arr = advice[i].id.split("|");
		
		//alert (search_arr.sort().join("|") + " --- " + advice_id_arr.sort().join("|"));
		if(search_arr.sort().join("|") == advice_id_arr.sort().join("|")){
			
			rs = advice[i];
			return rs;
		}
		
//		for(var y = 0; y < search_arr.length; y++){
//			var search_str = search_arr[y];
//			
//			for(var u = 0; u < advice_id_arr.length; u++){
//				//check if id exists in advice
//				//alert(search_str + ' ' + advice_id_arr[u]);
//				if(search_str == advice_id_arr[u]){
//					match ++; //there is match
//				}
//				
//				
//			}
//			
//		}
//		//alert(match + ' ' + advice_id_arr.length);
//		if(match == advice_id_arr.length){
//			rs = advice[i];
//			alert(match + ' ' + advice_id_arr.length);
//			dump(rs);
//			return rs;
//		}else{
//			match = 0;
//		}
	}
	
	
	//dump(rs);
	//alert(rs[0].code)
	//rs.length == 0 if no record
	
	//if record found, load next screen else report invalid
	if(rs.length == 0){
		return null;
	}else{
		return rs[0];
	}
}

function getessenstials(id) {
	//check code with server
	var rs;
	//jsonsql.query("select * from json.channel.items order by title desc", rs); 
	//console.log("ess id: " + id + " " + essentials[0].id);
	rs = jsonsql.query("select * from essentials where (id=='" + id + "')", essentials); 
	
	//console.log("rs arr: " + rs.length + "  " + rs[0].items[0].item);
	//dump(rs[0]);
	if(rs.length > 0)
	for(var i = 0; i < rs[0].items.length ; i++){
		
		essential_items.push(rs[0].items[i].item);
		
		//console.log("ess arr: " + rs[0].items[i].item);
	}
	
	//dump(rs);
	//alert(rs[0].code)
	//rs.length == 0 if no record
	
	//get advice into advice var
	advice = rs[0].advice;
	
	//console.log("ess advice: " + advice[0]);
	
	//if record found, load next screen else report invalid
	if(rs.length == 0){
		return null;
	}else{
		return essential_items;
	}
}

var lock_knock = false;
function knock_knock(code) {
	//check code with server
	if(isServer){
		jQuery.getJSON(domain_path + "RequestScript.php?nickname=wuzu&code="+code+"&req_type=knockknock", function(data, status){  
			//alert(status + " " + data);
			success_login = true;
			
			if(lock_knock) return;
			
			lock_knock = true;
			
		    // data is yours parsed object
			//alert(data[0].text);
			codes = data;
			
			updateWrapper('codes', codes);
			
			var rs;
			//jsonsql.query("select * from json.channel.items order by title desc", rs); 
			rs = jsonsql.query("select * from codes where (code=='" + code + "')", codes); 
			
			//dump(rs);
			//alert(rs[0].code)
			//rs.length == 0 if no record
			//if record found, load next screen else report invalid
			var granted;
			if(rs.length == 0){
				granted = false;
			}else{
				granted = true;
			}
			
			if(granted){
	    	
			  updateWrapper('code', code);
			  current_knock_code = code;
	  		  //prepare b4 transition
			  var user = fetchWrapper('user');
	  		  if(user == null)
	  			main.director.replaceScene(main.showLogin(), lime.transitions.MoveInUp);
	  		  else{
	  			  
	  			//FlurryAgent.setUserId(user.nickname); //TRACK
	  			setUser_analyticsWrapper(user.nickname);
	  			//setGender_analyticsWrapper(current_knock_code);
	  			
	  			main.director.replaceScene(main.showSplash(), lime.transitions.MoveInUp); //user didnt signout
	  		  }
	  	  }else{
	  		lock_knock = false;
	  		
	  		//analytics
	    	var user = fetchWrapper('user');
			var eventParameters = {};
			eventParameters['status'] = "Access Denied - " + code;
			
			logEvents_analyticsWrapper("knock_knock",
					eventParameters);
			
	  	  	alert("Access Denied");
	  	  }
			
		}).success(function() {  })
		.error(function() { alert("Server Error! Check your internet connection!"); })
		.complete(function() { });
	
	}else{
		//check code with server
		var rs;
		//jsonsql.query("select * from json.channel.items order by title desc", rs); 
		rs = jsonsql.query("select * from codes where (code=='" + code + "')", codes); 
		
		//dump(rs);
		//alert(rs[0].code)
		//rs.length == 0 if no record
		
		//if record found, load next screen else report invalid
		if(rs.length == 0){
			success_login = true;
			
			//analytics
	    	var user = fetchWrapper('user');
			var eventParameters = {};
			eventParameters['status'] = "Access Denied - " + code;
			
			logEvents_analyticsWrapper("knock_knock",
					eventParameters);
			
			return false;
		}else{
			updateWrapper('code', code);
			current_knock_code = code;
			success_login = true;
			return true;
		}
	}
}

var lock_login = false;
function login(nickname) {
	
//alert(isServer);
	if(isServer){
		//alert(domain_path + "RequestScript.php?nickname="+ nickname + "&code=wuzu&req_type=login");
	jQuery.getJSON(domain_path + "RequestScript.php?nickname="+ nickname + "&code=wuzu&req_type=login", function(data,status){    
		//alert(status + "  " + lock_login);
		success_login = true;
		
		if(lock_login) return;
		
		lock_login = true;
		
	//check code with server
		users = data;
		updateWrapper('users', users);	
		
	var rs;
	rs = jsonsql.query("select * from users where (nickname=='" + nickname + "')", users); 
	
	var granted;
	//if record found, load next screen else report invalid
	if(rs.length == 0){
		granted = false;
	}else{
		
		//visit server to download player data
		
		//story the json
		updateWrapper('user', rs[0]);
		
		granted = true;
	}
	
	if(granted){
		  
		  //prepare b4 transition
		updateWrapper('newlogin', "1");
		
			//FlurryAgent.setUserId(nickname);//TRACK
			setUser_analyticsWrapper(nickname);
			//setGender_analyticsWrapper(current_knock_code);
		  
		  main.director.replaceScene(main.showSplash(), lime.transitions.MoveInUp);
		  
	  }else{
		lock_login = false;
		
		//analytics
    	var user = fetchWrapper('user');
		var eventParameters = {};
		eventParameters['status'] = "Access Denied - " + nickname;
		
		logEvents_analyticsWrapper("login",
				eventParameters);
		
	  	alert("Access Denied");
	  }
   
	}).success(function() {  })
	.error(function() { alert("Server Error! Check your internet connection!"); })
	.complete(function() { });
	
	}else{
		success_login = true;
		//check code with server
		var rs;
		rs = jsonsql.query("select * from users where (nickname=='" + nickname + "')", users); 
		
		//if record found, load next screen else report invalid
		if(rs.length == 0){
			//analytics
	    	var user = fetchWrapper('user');
			var eventParameters = {};
			eventParameters['status'] = "Access Denied - " + nickname;
			
			logEvents_analyticsWrapper("login",
					eventParameters);
			
			return false;
		}else{
			
			//visit server to download player data
			//FlurryAgent.setUserId(nickname);//TRACK
			setUser_analyticsWrapper(nickname);
			setGender_analyticsWrapper(current_knock_code);
			
			//story the json
			updateWrapper('user', rs[0]);
			updateWrapper('newlogin', "1");
			
			return true;
		}
	}
}

var lock_register = false;
var invalid = false;
var cnt = 0;
var current_dialog;
function register(nickname, age, role) {
	// connectToJson();
	if(nickname == "" || nickname.toLowerCase() == "Enter Nickname".toLowerCase()){
		invalid = true;
		//alert("Please Enter Nickname");
		return -1;
	}
   
	if(age == "" || age == "Enter Age"){
		invalid = true;
		//alert("Please Enter Age");
		return -2;
	}
	
	if(role == "" || role.toLowerCase() == "Tap to select roles".toLowerCase()){
		invalid = true;
		//alert("Please Select roles");
		return -3;
	}
	
   if (invalid){ return; };
   
//	jQuery.getJSON("raw_codes_data.json", function(data){         
//	    // data is yours parsed object
//		//alert(data[0].text);
//		codes = data;
//	});
	if(isServer){
		$.ajaxSetup({async:false});
$.get(domain_path + "AddPlayer.php?nickname="+nickname+"&age="+age+"&role="+role +"",function(data,status){
  //alert("Data: " + data + "\nStatus: " + status);
	success_login = true;
	//alert(status + "  " + lock_login);
	if(lock_register) return;
	
	lock_register = true;
	
//load menu screen
	if(status == "success"){
		//main.loadMenuScene();
		//alert( data );
		if(data == "1"){
			//alert( "Sign up Successful! Proceed" );
			$.ajaxSetup({async:true});
			//main.showNote(current_dialog, "Sign up Successful! Login to Proceed", main.showLogin());
			main.showNote(current_dialog, "Sign up Successful! Logging in...", null);
			login(nickname);//temp
			return 1;
		}else{
			//alert( nickname + " already taken. please use a unique nickname" );
			main.showNote(current_dialog, nickname + " Nickname Exists", null);
			$.ajaxSetup({async:true});
			lock_register = false;
			return 0;
		}
		
		var eventParameters = {};
		eventParameters['nickname'] = nickname;
		eventParameters['age'] = age;
		eventParameters['role'] = role;
		eventParameters['code'] = current_knock_code;
		
//		FlurryAgent.logEvent("User_Data",
//				eventParameters);
		logEvents_analyticsWrapper("register", eventParameters);
		
	}else{
		lock_register = false;
		alert('Error connecting to server');
	}
  
}).success(function() {  })
.error(function() { alert("Server Error! Check your internet connection!"); })
.complete(function() { });
	}else{
		success_login = true;
		
		var eventParameters = {};
		eventParameters['nickname'] = nickname;
		eventParameters['age'] = age;
		eventParameters['role'] = role;
		eventParameters['code'] = current_knock_code;
		
//		FlurryAgent.logEvent("User_Data",
//				eventParameters);
		logEvents_analyticsWrapper("register", eventParameters);
		
		return (true);
	}
	
	
	//return (true);
}

function fetch_roles_server() {
		
	//json from server
    //list top 10 players
    //alert(json_data);
	//    var json_obj = JSON.parse(json_data);//(jsontestStr);
	//	//var size = json_obj[0].name;
	//	var y = 310;
	//	for(var i = 0; i < json_obj.length; i++){
	//		
	//		names_label = new lime.Label().setAlign('left').setFontColor('#0000ff').setFontSize(25)
	//	    .setSize(450, 35).setText(json_obj[i].name).setPosition(590,y);
	//		
	//		scores_label = new lime.Label().setAlign('center').setFontColor('#0000ff').setFontSize(25)
	//	    .setSize(450, 35).setText(json_obj[i].score).setPosition(900,y);
	//		
	//		var num = new lime.Label().setAlign('center').setFontColor('#0000ff').setFontSize(25)
	//	    .setSize(450, 35).setText(i+1 + ". ").setPosition(330,y);
	//		
	//		layer.appendChild(num);
	//		layer.appendChild(scores_label);
	//	    layer.appendChild(names_label);
	//	    
	//		y+= 40;
	//		}
	//    }
	
}

function startGame(){
	
  main.director.replaceScene(main.showKnock(), lime.transitions.MoveInUp);
	//test knock knock
//  var granted = knock_knock("d4300");
//  if(granted){
//	//alert("Access Granted");
//  }else{
//  	//alert("Access Denied");
//  }
  
	//test details
	//  register("wuzu", 33, "top");
	
	//login
	//  var granted = login("wuzu");
	//  if(granted){
	//	  alert("Access Granted");
	//  }else{
	//  	alert("Access Denied");
	//  }
	
	//playgame("wuzu","1");
}

function updateUser(json){
	
	updateWrapper('user', json);
	//store.set('user', json);	
	
//	  //store.set('user', { name: 'marcus', likes: 'javascript' });
//    // Get the stored object - store.js uses JSON.parse under the hood
//    	var user = store.get('user');
//    	if(user == null)
//    		alert("dd");
//    	alert(user.name + ' likes ' + user.likes)
}

function updateWrapper(key, value){
	store.set(key, value);
}

function fetchWrapper(key){
	return store.get(key);
}

function deleteWrapper(key){
	store.remove(key);
}

var isSyncing = false;
function updateUserObjToServerBG(){

var user = fetchWrapper('user');

//http://localhost/msm/UpdatePlayer.php?nickname=wuzu&step=t1&lastanswer=0&moretext=0&prevtype=0&current_step=0&anchor_step=0&fieldname=_
if(isServer){

if(isSyncing) return;  
//$.ajaxSetup({async:false});
$.get(domain_path + "UpdatePlayer.php?nickname="+ user.nickname +"&step="+ user.step +"&lastanswer="+ user.lastanswer +"&moretext="+user.moretext+"&prevtype="+user.prevtype+"&current_step="+user.current_step+"&anchor_step="+user.anchor_step+"&fieldname=_",function(data,status){
  //alert("Data: " + data + "\nStatus: " + status);
	//success_login = true;
	//alert(status + "  " + lock_login);
	if(isSyncing) return;
	
	isSyncing = true;
	
	if(status == "success"){
		//main.loadMenuScene();
		
		if(data == "User Updated!"){
			//alert( "Sign up Successful! Proceed" );
			//$.ajaxSetup({async:true});
			//main.showNote(current_dialog, "Sign up Successful! Login to Proceed", main.showLogin());
			//main.showNote(current_dialog, "Sign up Successful! Logging in...", null);
			//login(nickname);//temp
			isSyncing = false;
			//return 1;
		}else{
			//alert( nickname + " already taken. please use a unique nickname" );
			//main.showNote(current_dialog, nickname + " Nickname Exists", null);
			//$.ajaxSetup({async:true});
			isSyncing = false;
			//return 0;
		}
		
	}else{
		isSyncing = false;
		//alert('Error connecting to server');
	}
  
}).success(function() {  })
.error(function() { isSyncing = false; /*alert("Server Error! Check your internet connection!");*/ })
.complete(function() { });
	}
		
}

//abstract this to read from sqllite or mysql
function playgame(user, senderid) {
	
	 user_no = user;
     //user_no = user_no.trim();

     user_input = senderid;
     //user_input = user_input.trim();

     //var rs;
     //rs = jsonsql.query("select * from users where (nickname=='" + user + "')", users); 
 	 //dump(rs);
 	//alert(rs[0].code)
 	//rs.length == 0 if no record
 	
 	//if record found, load next screen else report invalid
 	//if(rs.length == 0){
     
     //get previous values of player
     //store.set('user', users);
     
     //alert("PP");
     
     var user = fetchWrapper('user');
     //dump(user);
     if(user == null){
 		  step = "0";
          last_answer = "0";
          more_step_text = "0";
          prev_type = "0";
          age = "0";
          role = "0";
          anchor_step = "0";
          
          //updateWrapper('user', users);
 	}else{
 		var selectedItems = fetchWrapper('selectedItems');
 		 //get the step value
        step = user.step;
        last_answer = user.lastanswer;
        more_step_text = selectedItems;//user.moretext;
        prev_type = user.prevtype;
        age = user.age;
        role = user.role;
        anchor_step = user.anchor_step;
        
        //alert(user.step);
 	}
 	 
 	
 	var rs;
 	//check if index is called with params and set as such.. thig might not work for redirecting games
 	
 	var step_param = gup( 'step' );
 	var anchor_step_param = gup( 'anchor_step' );
 	if(step_param != null && anchor_step_param != null){
 		step = step_param; anchor_step = anchor_step_param;
 		//check if user was created
 		isTesting = true;
 	}else{isTesting = false;}
 	//step = "quiz1end"; anchor_step = "quiz1";
 	//step = "t0"; anchor_step = "0";
 	console.log("prev step: " + step);
 	if(step.indexOf("|") > -1){ //only happens for type decision
 		step = user.laststep; //use this to take game to previous screen incase game terminates on decision
	 }
 	console.log("step: " + step);
 	
 	//check if call is coming from menu
 	if(senderid == "resume"){
 		//if(isTesting == false)
 		step = anchor_step;
 	}
 	
 	if(senderid == "restart"){
 		step = "0";
 	}
 	
 	if(senderid == "replay"){//use for game activities
 		step = anchor_step;
 	}
 	
	rs = jsonsql.query("select * from gameplay_nodes where (step=='" + step + "')", gameplay_nodes); 
	 
	//dump(rs);
	
	if(rs.length > 0){
		//check fields and play
		 var story = rs[0].text;
	     var pic = rs[0].pic;
	     var branches = rs[0].branches;
	     var no_of_branchs = rs[0].no_of_branchs;
	     var type = rs[0].type;
	     var expected_input = rs[0].expected_input;
	     var goto_step = rs[0].goto_step;
	     var snd = rs[0].snd;
	     anchor_step = rs[0].anchor_step;

	     //alert(story);
	     if(rs[0].type == 'narration'){//narration
	    	 
	    	 //output key is for text that the screens should work with
	    	 //split text for narration type
	    	 //narration text is in the format text|pic|sound ; text|pic|sound
	    	 //split inital into array and store in output for screen to do further work on it
	    	 var output = rs[0];//story.split(';');
	    	 updateWrapper('output', output);

             var lastanswer = "";
             //check if the previous was of type answer and upadte lastanswer
             if (prev_type == 1) {//if prev is answer
             	lastanswer = user_input;
                 //track(user_no,Integer.parseInt(step),Integer.parseInt(user_input), st);
             } else {
                 lastanswer = "";
             }

             var current_step = step;
             //increase step
             if(goto_step == ""){
            	 step = parseInt(parseInt(step) + 1);
             }else{
            	 step = goto_step;
             }
             
             
             //update step and more
             //int i = st.executeUpdate("UPDATE users SET " + lastanswer + "prevtype='" + type + "', step='" + step + "', moretext='1' WHERE userid='" + user_no + "'");

             user.step = step;
             user.lastanswer = last_answer ;
             user.moretext = more_step_text ;
             user.prevtype = type;
             user.current_step = current_step;
             user.anchor_step = anchor_step;
             
             //dump(user);
             
             updateWrapper('user', user);
             
             updateUserObjToServerBG();
             
             main.director.replaceScene(main.dialogs.NarrationBoxTemplate(), lime.transitions.MoveInUp);
	    	 
	     }else if(rs[0].type == 'decision'){//decision
	    
	    	 //prepare data
	    	 //dump(rs);
	    	 var output = rs[0];
	    	 
	    	 //split and save the goto values for yes and no
	    	 if(goto_step.indexOf("|") > -1){
	    		 var ans = goto_step.split("|");
	    		 yes_step = ans[0];
	    		 no_step = ans[1];
	    		 decision_choices = ans;
	    		 user.laststep = step; //use this to take game to previous screen incase game terminates on decision
	    	 }
	    		 
	    	 updateWrapper('output', output);
	    	 //updateWrapper('output', rs[0]);

             var lastanswer = "";
             //check if the previous was of type answer and upadte lastanswer
             if (prev_type == 1) {//if prev is answer
             	lastanswer = user_input;
                 //track(user_no,Integer.parseInt(step),Integer.parseInt(user_input), st);
             } else {
                 lastanswer = "";
             }

             var current_step = step;
             //increase step
             if(goto_step == ""){
            	 step = parseInt(parseInt(step) + 1);
             }else{
            	 step = goto_step;
             }
             
             //update step and more
             //int i = st.executeUpdate("UPDATE users SET " + lastanswer + "prevtype='" + type + "', step='" + step + "', moretext='1' WHERE userid='" + user_no + "'");

             user.step = step;
             user.lastanswer = last_answer ;
             user.moretext = more_step_text ;
             user.prevtype = type;
             user.current_step = current_step;
             user.anchor_step = anchor_step;
             
             updateWrapper('user', user);
             
             updateUserObjToServerBG();
             
             //screen transition
	    	 
             main.director.replaceScene(main.dialogs.twoWayDecisionBoxTemplate(), lime.transitions.MoveInUp);
             
	     }else if(rs[0].type == 'dialogue'){//dialogue
	    	 
	    	//prepare data
	    	 updateWrapper('output', rs[0]);

             var lastanswer = "";
             //check if the previous was of type answer and upadte lastanswer
             if (prev_type == 1) {//if prev is answer
             	lastanswer = user_input;
                 //track(user_no,Integer.parseInt(step),Integer.parseInt(user_input), st);
             } else {
                 lastanswer = "";
             }
             
             var current_step = step;
             //increase step
             if(goto_step == ""){
            	 step = parseInt(parseInt(step) + 1);
             }else{
            	 step = goto_step;
             }
             
             //update step and more
             //int i = st.executeUpdate("UPDATE users SET " + lastanswer + "prevtype='" + type + "', step='" + step + "', moretext='1' WHERE userid='" + user_no + "'");

             user.step = step;
             user.lastanswer = last_answer ;
             user.moretext = more_step_text ;
             user.prevtype = type;
             user.current_step = current_step;
             user.anchor_step = anchor_step;
             
             updateWrapper('user', user);
             
             updateUserObjToServerBG();
	    	 
             //screen transition
	    	 
             main.director.replaceScene(main.dialogs.DialogBoxTemplate(), lime.transitions.MoveInUp);
             
	     }else if(rs[0].type == 'advice'){//advice
	    	 
	    	 //var essentials = rs[0].check_essentials;
	    	//prepare data
	    	 updateWrapper('output', rs[0]);

             var lastanswer = "";
             //check if the previous was of type answer and upadte lastanswer
             if (prev_type == 1) {//if prev is answer
             	lastanswer = user_input;
                 //track(user_no,Integer.parseInt(step),Integer.parseInt(user_input), st);
             } else {
                 lastanswer = "";
             }

             var current_step = step;
             //increase step
             if(goto_step == ""){
            	 step = parseInt(parseInt(step) + 1);
             }else{
            	 step = goto_step;
             }
             
             //update step and more
             //int i = st.executeUpdate("UPDATE users SET " + lastanswer + "prevtype='" + type + "', step='" + step + "', moretext='1' WHERE userid='" + user_no + "'");

             user.step = step;
             user.lastanswer = last_answer ;
             user.moretext = more_step_text ;
             user.prevtype = type;
             user.current_step = current_step;
             user.anchor_step = anchor_step;
             
             updateWrapper('user', user);
             
             updateUserObjToServerBG();
	    	 
             main.director.replaceScene(main.dialogs.AdviceBoxTemplate(), lime.transitions.MoveInUp);
	    	 
	     }else if(rs[0].type == 'multichoice'){//multichoice
	    	 
	    	 //var essentials = rs[0].check_essentials;
	    	//prepare data
	    	 updateWrapper('output', rs[0]);

             var lastanswer = "";
             //check if the previous was of type answer and upadte lastanswer
             if (prev_type == 1) {//if prev is answer
             	lastanswer = user_input;
                 //track(user_no,Integer.parseInt(step),Integer.parseInt(user_input), st);
             } else {
                 lastanswer = "";
             }

             var current_step = step;
             //increase step
             if(goto_step == ""){
            	 step = parseInt(parseInt(step) + 1);
             }else{
            	 step = goto_step;
            	 //alert(step);
             }
             
             //update step and more
             //int i = st.executeUpdate("UPDATE users SET " + lastanswer + "prevtype='" + type + "', step='" + step + "', moretext='1' WHERE userid='" + user_no + "'");

             user.step = step;
             user.lastanswer = last_answer ;
             user.moretext = more_step_text ;
             user.prevtype = type;
             user.current_step = current_step;
             user.anchor_step = anchor_step;
             
             updateWrapper('user', user);
             
             updateUserObjToServerBG();
             
             main.director.replaceScene(main.dialogs.multipleTaskBoxTemplate(), lime.transitions.MoveInUp);
	     }
	     
	     else if(rs[0].type == 'singlechoice'){//single select
	    	 //0 = choice1, 1 = choice2, 2 = choice3, 3 = ...
	    	//split and save the goto values for e.g choice1 = yes and choice2 = no
	    	 if(goto_step.indexOf("|") > -1){
	    		 var ans = goto_step.split("|");
	    		 yes_step = ans[0];
	    		 no_step = ans[1];
	    		 decision_choices = ans;
	    		 user.laststep = step; //use this to take game to previous screen incase game terminates on decision
	    	 }
		    	//prepare data
		    	 updateWrapper('output', rs[0]);

	             var lastanswer = "";
	             //check if the previous was of type answer and upadte lastanswer
	             if (prev_type == 1) {//if prev is answer
	             	lastanswer = user_input;
	                 //track(user_no,Integer.parseInt(step),Integer.parseInt(user_input), st);
	             } else {
	                 lastanswer = "";
	             }

	             var current_step = step;
	             //increase step
	             if(goto_step == ""){
	            	 step = parseInt(parseInt(step) + 1);
	             }else{
	            	 step = goto_step;
	             }
	             
	             //update step and more
	             //int i = st.executeUpdate("UPDATE users SET " + lastanswer + "prevtype='" + type + "', step='" + step + "', moretext='1' WHERE userid='" + user_no + "'");

	             user.step = step;
	             user.lastanswer = last_answer ;
	             user.moretext = more_step_text ;
	             user.prevtype = type;
	             user.current_step = current_step;
	             user.anchor_step = anchor_step;
	             
	             updateWrapper('user', user);
	             
	             updateUserObjToServerBG();
	             
	             main.director.replaceScene(main.dialogs.singleTaskBoxTemplate(), lime.transitions.MoveInUp);
		     
	     }else if(rs[0].type == 'end'){//single select
	    	 //0 = choice1, 1 = choice2, 2 = choice3, 3 = ...
		    	//split and save the goto values for e.g choice1 = yes and choice2 = no
		    	
			    	//prepare data
			    	 updateWrapper('output', rs[0]);

		             var lastanswer = "";
		             //check if the previous was of type answer and upadte lastanswer
		             if (prev_type == 1) {//if prev is answer
		             	lastanswer = user_input;
		                 //track(user_no,Integer.parseInt(step),Integer.parseInt(user_input), st);
		             } else {
		                 lastanswer = "";
		             }
		             
		             var current_step = step;
		             //increase step
		             if(goto_step == ""){
		            	 step = parseInt(parseInt(step) + 1);
		             }else{
		            	 step = goto_step;
		             }
		             
		             //check end of game
			            if(step == "-1"){
			            	//alert(step);
			            	//main.director.replaceScene(main.dialogs.endBoxTemplate(), lime.transitions.MoveInUp);
			            	 main.loadMenuScene();
			            	 return 0;
			            }
		             
		             //update step and more
		             //int i = st.executeUpdate("UPDATE users SET " + lastanswer + "prevtype='" + type + "', step='" + step + "', moretext='1' WHERE userid='" + user_no + "'");

		             user.step = step;
		             user.lastanswer = last_answer ;
		             user.moretext = more_step_text ;
		             user.prevtype = type;
		             user.current_step = current_step;
		             user.anchor_step = anchor_step;
		             
		             updateWrapper('user', user);
		             
		             updateUserObjToServerBG();
		             
		             main.director.replaceScene(main.dialogs.endBoxTemplate(), lime.transitions.MoveInUp);
			     
		     }
	     else if(rs[0].type == 'activity'){//single select
   	 
	    	//prepare data
	    	 updateWrapper('output', rs[0]);

            var lastanswer = "";
            //check if the previous was of type answer and upadte lastanswer
            if (prev_type == 1) {//if prev is answer
            	lastanswer = user_input;
                //track(user_no,Integer.parseInt(step),Integer.parseInt(user_input), st);
            } else {
                lastanswer = "";
            }
            var current_step = step;
            //increase step
            if(goto_step == ""){
           	 step = parseInt(parseInt(step) + 1);
            }else{
           	 step = goto_step;
            }

            //update step and more
            //int i = st.executeUpdate("UPDATE users SET " + lastanswer + "prevtype='" + type + "', step='" + step + "', moretext='1' WHERE userid='" + user_no + "'");

            user.step = step;
            user.lastanswer = last_answer ;
            user.moretext = more_step_text ;
            user.prevtype = type;
            user.current_step = current_step;
            user.anchor_step = anchor_step;
            
            updateWrapper('user', user);
            
            updateUserObjToServerBG();
            
           main.director.replaceScene(main.dialogs.puzzleGameTemplate(), lime.transitions.MoveInUp);
            
	     }
	     else if(rs[0].type == 'activityend'){//single select
      	 
    	//prepare data
    	 updateWrapper('output', rs[0]);

        var lastanswer = "";
        //check if the previous was of type answer and upadte lastanswer
        if (prev_type == 1) {//if prev is answer
        	lastanswer = user_input;
            //track(user_no,Integer.parseInt(step),Integer.parseInt(user_input), st);
        } else {
            lastanswer = "";
        }
        var current_step = step;
        //increase step
        if(goto_step == ""){
       	 step = parseInt(parseInt(step) + 1);
        }else{
       	 step = goto_step;
        }

        //update step and more
        //int i = st.executeUpdate("UPDATE users SET " + lastanswer + "prevtype='" + type + "', step='" + step + "', moretext='1' WHERE userid='" + user_no + "'");

        user.step = step;
        user.lastanswer = last_answer ;
        user.moretext = more_step_text ;
        user.prevtype = type;
        user.current_step = current_step;
        user.anchor_step = anchor_step;
        
        updateWrapper('user', user);
        
        updateUserObjToServerBG();
        
       main.director.replaceScene(main.dialogs.puzzleGameEndTemplate(), lime.transitions.MoveInUp);
        
     }
	     
	}
	
	
     
     //send data to server
     //lt_ajax_getMTN(domain_path + 'AddScore.php?name=' + arrdata[1] +'&score=' + arrdata[0] +'&number=' + score +'&hash=' + hash +'&level=' + level, false);
     
     
}

function logEvents_analyticsWrapper(id) {
	//analytics
	//var eventParameters = params;
	//eventParameters['choice'] = path1_layer.id;
	
	//window.plugins.flurry.logEvent(id);
	if(isTesting == false)
	//flurry.logEvent(id);
	window.plugins.flurry.logEvent(id);
	
}

function logEvents_analyticsWrapper(id, params) {
	//analytics
	var eventParameters = params;
	//eventParameters['choice'] = path1_layer.id;
	
	//window.plugins.flurry.logEvent(id, eventParameters);
	if(isTesting == false)
	window.plugins.flurry.logEvent(id, eventParameters);
	
}

function setUser_analyticsWrapper(id) {
	//analytics
	//window.plugins.flurry.setUserID(id);//TRACK
	if(isTesting == false)
	window.plugins.flurry.setUserID(id);//TRACK
	
}

function setGender_analyticsWrapper(id) {
	//analytics
	//window.plugins.flurry.setGender(id);//TRACK
	if(isTesting == false)
	window.plugins.flurry.setGender(id);//TRACK
	
	//analytics
	var user = fetchWrapper('user');
	var eventParameters = {};
	eventParameters['parent_code'] = id;
	eventParameters['parent_code'] = user.nickname;
	
	logEvents_analyticsWrapper("SecretCode",
			eventParameters);
	
}

function startSession_analyticsWrapper(id) {
	//analytics
	//FlurryAgent.startSession(id);
	//window.plugins.flurry.startSession(id);
	if(isTesting == false){
		window.plugins.flurry.setSessionReportsOnCloseEnabled();
        window.plugins.flurry.setSessionReportsOnPauseEnabled();
	
		//flurry.startSession(id);

	}
//setSessionReportsOnCloseEnabled and setSessionReportsOnPauseEnabled	
}


///mobile web
// function logEvents_analyticsWrapper(id) {
// 	//analytics
// 	//var eventParameters = params;
// 	//eventParameters['choice'] = path1_layer.id;
// 	if(isTesting == false)
// 	FlurryAgent.logEvent(id);
// 	
// }
// 
// function logEvents_analyticsWrapper(id, params) {
// 	//analytics
// 	var eventParameters = params;
// 	//eventParameters['choice'] = path1_layer.id;
// 	
//if(isTesting == false)
// 	FlurryAgent.logEvent(id,
// 			eventParameters);
// 	
// }
// 
// function setUser_analyticsWrapper(id) {
// 	//analytics
//if(isTesting == false)
// 	FlurryAgent.setUserId(id);//TRACK
// 	
// }
// 
// function setGender_analyticsWrapper(id) {
// 	//analytics
//if(isTesting == false)
// 	FlurryAgent.setGender(id);//TRACK
// 	
// 	//analytics
// 	var user = fetchWrapper('user');
// 	var eventParameters = {};
// 	eventParameters['parent_code'] = id;
// 	eventParameters['parent_code'] = user.nickname;
// 	
// 	logEvents_analyticsWrapper("SecretCode",
// 			eventParameters);
// 	
// }
// 
// function startSession_analyticsWrapper(id) {
// 	//analytics
//if(isTesting == false)
// 	FlurryAgent.startSession(id);
// 	
// }
