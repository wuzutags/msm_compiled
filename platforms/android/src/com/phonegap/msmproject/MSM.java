/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.phonegap.msmproject;

import android.os.Bundle;
import org.apache.cordova.*;
import com.flurry.android.FlurryAgent;

public class MSM extends CordovaActivity 
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        super.init();
        // Set by <content src="index.html" /> in config.xml
        //super.loadUrl(Config.getStartUrl());
        super.loadUrl("file:///android_asset/www/index.html");
        //super.loadUrl("http://192.168.1.108:8888/voda/the_ceo/limejs/lime/the_ceo/login.php");
    }
    
    @Override
protected void onStart()
{
    super.onStart();
    FlurryAgent.onStartSession(this, "ZY6VQCPC9QWZDMPKFPJF");
}

@Override
protected void onStop()
{
    super.onStop();
    FlurryAgent.onEndSession(this);
}
}

/*
	This has the closure files also included.
	Use this project for code edits. To compile, copy all files in streetsoccer folder in assets/www into 
	/Users/donald/NetBeansProjects/mrVodafoneCEO/voda/the_ceo/limejs/lime/streetsoccer
	
	use this to compile
	../../bin/lime.py build streetsoccer -o ../../streetsoccer_compiled/streetsoccer.js -p streetsoccer.start
	
	copy back the output into GhanaRioGames project and deploy

*/